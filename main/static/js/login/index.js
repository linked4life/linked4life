(function(){
	var donor = true;

	$("button.btn.login").on("click", function(e) {
		var emailid = $("input[name=email]").val();
		location.pathname = "/users/"+emailid;
	});

	$(".type").change(function(e) {
		donor = $(this).prop('checked');
	});

	$(".submit-form").on("click", function(e) {
		var data = {};
		data['first_name'] = $("#first_name").val();
		data['last_name'] = $("#last_name").val();
		data['email'] = $("#email").val();
		data['age'] = $("#age").val();
		data['zipcode'] = $("#zipcode").val();
		data['blood_type'] = $("select[name=blood-type]").val();
		data['user_type'] = donor? "donor":"recipient";

		$.ajax({
			url: "/saveuser",
			data: data,
			method: "POST"
		}).done(function(data) {
			var url = location.origin;
			location.href = url+"/";
		});
	});
})();