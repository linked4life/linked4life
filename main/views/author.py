from flask import Flask, request, render_template, redirect, url_for, send_from_directory, jsonify, Blueprint, abort
from jinja2 import TemplateNotFound
from main.dbaccess import DBAccess
from main.utils import db_ready_string, handle_uploads, parse_save_master_file, parse_question
from students import update_students_test
import json, re

author = Blueprint('author', __name__)

@author.route('/<term>/<year>/<classname>/<test>/author', methods=['GET', 'POST'])
def upload_master_file(term, year, classname, test):
	db = DBAccess()
	url = "/".join([term, year, classname, test])
	if request.method == 'POST':
		file_object = request.files['file']

		filename = handle_uploads(file_object)
		if filename != None:
			no_of_questions = parse_save_master_file(filename, term, int(year), classname, test)
			update_students_test(term, int(year), classname, test, no_of_questions)

		return redirect('/'+url+'/author/show')

	term_test = db.get_test(term, int(year), classname, test)
	if "questions" in term_test:
		return redirect('/'+url+'/author/show')

	return render_template("author/upload.html", details=term_test)



@author.route('/<term>/<year>/<classname>/<test>/author/show')
def visualize_concepts(term, year, classname, test):
	return render_template('author/author_overview.html')


@author.route('/<term>/<year>/<classname>/<test>/author/author_mode')
def authoring_dashboard(term, year, classname, test):
	db = DBAccess()
	year = int(year)

	questions = db.get_all_questions(term, year, classname, test)
	answers = db.get_all_answers(term, year, classname, test)
	concepts = db.get_all_question_concepts(term, year, classname, test)
	scores = db.get_all_question_scores(term, year, classname, test)
	higher_concepts = db.get_higher_concepts()

	return render_template('author/author_mode.html', questions=questions["questions"], concepts=concepts["concepts"], higher_concepts=higher_concepts["higherconcepts"], answers=answers["answers"], scores=scores["scores"])

# @author.route('/<term>/<year>/<classname>/<test>/author/export_changes', methods=['POST'])
# def export_changes():
# 	db = DBAccess()

	

# @author.route('/<term>/<year>/<classname>/<test>/author/author_mode')
# def authoring_dashboard(term, year, classname, test):
# 	db = DBAccess()
# 	year = int(year)

# 	questions = db.get_all_questions(term, year, classname, test)
# 	answers = db.get_all_answers(term, year, classname, test)
# 	concepts = db.get_all_question_concepts(term, year, classname, test)
# 	higher_concepts = db.get_higher_concepts()

# 	return render_template('author/author_mode.html', questions=questions["questions"], concepts=concepts["concepts"], higher_concepts=higher_concepts["higherconcepts"], answers=answers["answers"])


@author.route('/<term>/<year>/<classname>/<test>/author/author_mode/update_question', methods=['POST'])
def update_question(term, year, classname, test):
	db = DBAccess()
	data = json.loads(request.data)
	question = re.sub(r'\t|\s\s+', '', data['question'])
	answer = data['answer']
	score = data['score']
	question_number = data['qnum']


	key = {"term": term, "year": int(year), "class_name": classname}
	db.update_question(key, test, question_number, question, answer, float(score))

	concepts = parse_question(question)
	key["name"] = test
	concepts = db.update_question_concept(key, question_number, concepts)
	return jsonify(data=concepts)


@author.route('/<term>/<year>/<classname>/<test>/author/author_mode/update_concepts', methods=['POST'])
def update_concepts(term, year, classname, test):
	db = DBAccess()
	data = json.loads(request.data)
	qconcepts = data['concepts']
	question_number = data['qnum']

	key = {"term": term, "year": int(year), "class_name": classname, "name": test}
	db.update_question_concept(key, question_number, qconcepts, True)

	return "OK"
