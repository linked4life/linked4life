from flask import Flask, request, render_template, redirect, url_for, send_from_directory, jsonify, Blueprint, abort
from jinja2 import TemplateNotFound
from main.dbaccess import DBAccess
from main.model.user import User
from register_form import RegistrationForm
import json, re

login = Blueprint('login', __name__)

@login.route("/")
def login_page():
	return render_template("login/login.html")

@login.route("/register")
def register():
	return render_template("login/donor_register.html")

@login.route("/saveuser", methods=["POST"])
def save_user():

	first_name = request.form.get("first_name")
	last_name = request.form.get("last_name")
	email = request.form.get("email")
	age = request.form.get("age")
	zipcode = request.form.get("zipcode")
	user_type = request.form.get("user_type")
	blood_type = request.form.get("blood_type")

	user = User(email, first_name, last_name, age, zipcode, user_type, blood_type)
	user.save()

	return redirect('/')
