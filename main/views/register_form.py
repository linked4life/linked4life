from flask_wtf import Form
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired, NumberRange, Email

class RegistrationForm(Form):
	first_name = StringField('First Name', validators=[DataRequired("Please enter your first name")])
	last_name = StringField('Last Name', validators=[DataRequired("Please enter your last name")])
	email = StringField('Email id', validators=[DataRequired("Please enter emailid"), Email("Email address is incorrect")])
	age = IntegerField('Age', [NumberRange(min=18, max=65)])
	zipcode = IntegerField('Zipcode', [NumberRange(min=5, max=5)])
	user_type = StringField('Type', validators=[DataRequired("Please select your type")])
	blood_type = StringField('Blood type', validators=[DataRequired("Please select your blood type")])
