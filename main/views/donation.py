from flask import Flask, request, render_template, redirect, url_for, send_from_directory, jsonify, Blueprint, abort
from jinja2 import TemplateNotFound
from main.dbaccess import DBAccess
from main.model.user import User
from main.linkforlife import Linkforlife
import json, re

donation = Blueprint('donation', __name__)

@donation.route("/users/<email>")
def show_listings(email):
	link = Linkforlife()
	db = DBAccess()
	user = db.get_user(email)

	if user["user_type"] == "donor":
		donor = db.get_donor(user["email"])
		chains = []
		if "chained_blood_type" in donor:
			chains = link.find_chain_recipient_match(donor)

		listings = link.find_recipients(donor)
		
		return render_template("donation/donor.html", chains=chains, listings=listings)

	recipient = db.get_recipient(email)
	offers = link.find_offers(email)
	others = link.find_recipients(recipient)

	return render_template("donation/recipient.html", offers=offers, others=others)

@donation.route("/search")
def search():
	link = Linkforlife()
	email = request.args.get("email")
	recipient = link.get_recipient(email)

	return redirect("/recipients/"+recipient["id"])

@donation.route("/recipients/<email>")
def recipient_profile(email):
	user = db.get_recipient(email)
	return render_template("donation/recipient_profile.html", user=user)