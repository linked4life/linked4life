import os, config
from flask import Flask, render_template, send_from_directory
from pymongo import MongoClient

app = Flask(__name__)
app.debug = config.DEBUG

mongo = MongoClient(config.MONGO_URL)

from views.login import login
from views.donation import donation

app.register_blueprint(login)
app.register_blueprint(donation)

# from utils import split_lines
# app.jinja_env.filters['split_lines'] = split_lines

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'images/favicon.ico')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404



