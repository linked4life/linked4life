from dbaccess import DBAccess

class Linkforlife(object):
	def __init__(self):

		self.db = DBAccess()

		self.recipient_compatibility = {
			"AB+": ["O-", "O+", "B-", "B+", "A-", "A+", "AB-", "AB+"],
			"AB-": ["O-", "B-", "A-", "AB-"],
			"A+": ["O-", "O+", "A-", "A+"],
			"A-": ["O-", "A-"],
			"B+": ["O-", "O+", "B+", "B-"],
			"B-": ["O-", "B-"],
			"O+": ["O-", "O+"],
			"O-": ["O-"]
		}

		self.donor_compatibility = {
			"O-": ["O-", "O+", "B-", "B+", "A-", "A+", "AB-", "AB+"],
			"O+": ["O+", "B+", "A+", "AB+"],
			"B-": ["B-", "B+", "AB+", "AB-"],
			"B+": ["B+", "AB+"],
			"A-": ["A-", "A+", "AB+", "AB-"],
			"A+": ["A+", "AB+"],
			"AB-": ["AB+", "AB-"],
			"AB+": ["AB+"]
		}

		# db.chained_donor_list.find({})
		chain_donor_list = [
			{"email": "asd@as.g", "blood_type": "A+", "chained_blood_type": "B+"},
			{"email": "asd1@as.g", "blood_type": "O-", "chained_blood_type": "A+"},
			{"email": "asd2@as.g", "blood_type": "O+", "chained_blood_type": "A-"},
			{"email": "asd3@as.g", "blood_type": "B+", "chained_blood_type": "A+"}
		]

		self.chain = {}
		self.swap = {}

		self.swap_order = []
		self.chain_order = []

	def get_recipient(self, email):
		return self.db.get_recipient(email)

	def find_recipients(self, user):
		user_blood_type = user["blood_type"]
		return self.db.find_recipients(user_blood_type)

	def find_offers(self, email):
		return self.db.find_offers(email)

	def find_chain_recipient_match(self, donor):
		donor_blood_type = donor["blood_type"]
		chain[donor["email"]] = donor
		swap[donor["email"]] = donor
		recipient_blood_type = donor["chained_blood_type"]

		donor_id = donor["email"]
		chain_order.append(donor_id)
		swap_order.append(donor_id)

		self.find_chains(donor_blood_type, recipient_blood_type, donor_id)

		return {"chain": chain, "chain_order": chain_order, "swap": swap, "swap_order": swap_order}


	def find_chains(self, donor_bt, recipient_bt, exclude_id):

		direct_swaps, middle_men = self.find_compatible_donors(donor_bt, recipient_bt, exclude_id)
		recipients_blood_type = [donor["chained_blood_type"] for donor in middle_men]

		for idx in range(0,len(recipients_blood_type)):
			second_level = self.find_compatible_donors(donor_bt, recipients_blood_type[idx], exclude_id, True)
			if len(second_level) == 1:
				self.chain_order.append(middle_men[idx]["email"])
				self.chain[middle_men[idx]["email"]] = middle_men[idx]
				
				self.chain_order.append(second_level[0]["email"])
				self.chain[second_level[0]["email"]] = second_level[0]

		for dswap in direct_swaps:
			self.swap[dswap["email"]] = dswap
			self.swap_order.append(dswap["email"])


	def find_compatible_donors(self, dblood_type, rblood_type, exclude_id, only_swaps=False):
		compatible_blood_types = self.recipient_compatibility[rblood_type]
		compatible_dblood_types = self.donor_compatibility[dblood_type]
		users = filter(lambda x: x["blood_type"] in compatible_blood_types, self.chain_donor_list)#db.chained_donor_list.find({"blood_type": {"$in": compatible_blood_types}})
		swaps = filter(lambda x: x["chained_blood_type"] in compatible_dblood_types, users)

		if not only_swaps:
			for_q = filter(lambda x: x["chained_blood_type"] not in compatible_dblood_types, users)
			return swaps, for_q

		return swaps
