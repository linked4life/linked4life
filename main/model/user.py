from main.dbaccess import DBAccess
from main.utils import create_db_string

class User(object):
	def __init__(self, email, first_name=None, last_name=None, age=None, zipcode=None, user_type=None, blood_type=None, karma=100):
		self.email = email
		self.first_name = create_db_string(first_name)
		self.display_first_name = first_name
		self.last_name = create_db_string(last_name)
		self.display_last_name = last_name
		self.age = age
		self.zipcode = zipcode
		self.user_type = user_type
		self.blood_type = blood_type
		self.karma = karma

	def get_id(self):
		db = DBAccess()
		return db.get_id(self.email, self.user_type)

	def save(self):
		db = DBAccess()
		if self.user_type == "donor":
			db.save_donor(self)
		else:
			db.save_recipient(self)
		return "OK"