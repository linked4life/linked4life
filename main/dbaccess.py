from main import mongo
import pdb
class DBAccess(object):
	def __init__(self):
		self.db = mongo.linkd4life
		self.users = self.db.users
		self.donors = self.db.donors
		self.recipients = self.db.recipients
		self.offers = self.db.offers

	def save_donor(self, user):
		self.donors.insert(user.__dict__)
		self.users.insert(user.__dict__)

	def save_recipient(self, user):
		self.recipients.insert(user.__dict__)
		self.users.insert(user.__dict__)

	def get_id(self, email_id, user_type):
		key = {"email": email_id}
		return self.users.find_one(key, {"email": 1})

	def get_user(self, email):
		key = {"email": email}
		return self.users.find_one(key)

	def get_donor(self, email):
		key = {"email": email}
		return self.donors.find_one(key)

	def get_recipient(self, email):
		key = {"email": email}
		return self.recipients.find_one(key)

	def find_recipients(self, donor_blood_type):
		key = {"blood_type": donor_blood_type}
		return list(self.recipients.find(key))

	def find_offers(self, email):
		key={"emailTo": email}
		return list(self.offers.find(key))